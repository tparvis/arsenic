#!/usr/bin/env python3
import IPython
import re
import random
import time
from selenium import webdriver

from .Task import *

class Google(Task):
    def home(self):
        self.dr.get("http://google.com")

    def is_open(self):
        return re.match(r'https?://.*\.google\.com/?.*', self.driver.current_url)

    def search(self, term):
        self.home()

        input_elements = driver.find_elements_by_tag_name("input")
        input_search = list(filter(lambda elem: elem.is_displayed(), input_elements))[0]
       
        self.focus(input_search, click=True)
        input_search.send_keys("")
       
        time.sleep(random.uniform(1,3))
        self.human_send_keys(term)


    def get_search_results(self):
        search_results = []
        for h3 in driver.find_elements_by_class_name("r"):
            search_results.append(h3.find_elements_by_tag_name("a")[0])
        return search_results


    def click_random_result(self):
        link = random.choice(self.get_search_results())
        self.focus(link, click=True)


if __name__ == "__main__":
    driver = webdriver.Firefox()
    g = Google(driver)
    g.search("Hello World")
    time.sleep(random.uniform(1,3))
    g.click_random_result()

# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:

