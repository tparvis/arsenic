#!/usr/bin/env python3

import re
import random
import time
from .Task import *


class Wikipedia(Task):
    def __init__(self, driver, lang="en"):
        Task.__init__(self, driver)
        self.lang = lang

    def home(self):
        self.dr.get("http://{}.wikipedia.org".format(self.lang))

    def is_open(self):
        return re.match(r'https?://.*?\.wikipedia\.org/?.*', self.dr.current_url) != None

    def search(self, term):
        s = self.dr.find_element_by_id("searchInput")
        s.send_keys(str(term))
        s.submit()

    def follow(self):
        links = self.get_links()
        link = None
        while link == None:
            link = random.choice(links)
            if not (link[2].is_displayed() and re.match(r'https?://' + self.lang + '\.wikipedia\.org/?.*', link[0])):
                link = None
            else:
                link[2].click()


class BWikipediaBrowser(Task):
    def __init__(self, driver):
        super(BWikipediaBrowser, self).__init__(driver)
        self.w = Wikipedia(self.dr)

    def __next__(self):
        if not self.w.is_open():
            self.w.home()
        self.w.follow()
        time.sleep(random.randrange(1,5))
        
        

# Test
if __name__ == "__main__":
    import selenium
    from selenium import webdriver
    driver = webdriver.Firefox()
    
    #wiki = Wikipedia(driver)
    #wiki.home()
    #wiki.search("Goat")
    #for i in range(0, 100):
    #    wiki.follow()

    b = BWikipediaBrowser(driver)
    for i in range(0,10):
        next(b)

# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:

