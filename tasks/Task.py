#!/usr/bin/env python3
import time
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from humanize import typist


class Task(object):
    duration = 0    #: Time [s] that a normal user spends on this task
    children = []
    weight = 1

    def __init__(self, driver):
        self.dr = driver

    #: retrieve all hyperlinks in the current document
    #: -> List [[href, linktext, entity_object], ...]
    def get_links(self):
        return self.dr.execute_script('''var result = Array(); var x = document.links; for(i=0; i<x.length; i++) { result.push(Array(x[i].href, x[i].text, x[i])); }; return result;''')

    #: retrieve all hyperlinks in the current document (faster)
    #: -> List [[href, linktext], ...]
    def get_links_fast(self):
        return self.dr.execute_script('''var result = Array(); var x = document.links; for(i=0; i<x.length; i++) { result.push(Array(x[i].href, x[i].text)); }; return result;''')

    
    def human_send_keys(self, text):
        def key_func(c):
            action = webdriver.ActionChains(self.dr)
            if c == "\n":
                action.send_keys(Keys.RETURN)
            elif c == "\b":
                action.send_keys(Keys.BACKSPACE)
            else:
                action.send_keys(c)
            action.perform()
        typist.simulate_typing(text, key_function=key_func)


    def send_key(self, key):
        def key_func(c):
           action = webdriver.ActionChains(self.dr)
           action.send_keys(key)
           action.perform()
        typist.simulate_typing("@", key_function=key_func)
        

    def focus(self, element, click=False):
        time.sleep(random.uniform(0,5))
        action = webdriver.ActionChains(self.dr)
        action.move_to_element_with_offset(element, random.randrange(2,8), random.randrange(2,6))
        action.perform()
        if click:
            time.sleep(random.uniform(0.2,1))
            action = webdriver.ActionChains(self.dr)
            action.click()
            action.perform()
            


class TaskGroup(Task):
    #: get the next task in line
    def __next__(self):
        pass


class TaskGroupLinear(Task):
    def next():
        if (len(self.children) > 0):
            return self.children.pop(0)
        else:
            return None


class TaskGroupRandom(Task):
    def _weighted_choice(self, choices):
       total = sum(c.weight for c in choices)
       r = random.uniform(0, total)
       upto = 0
       for c in choices:
          if upto + c.weight > r:
             return c
          upto += w
       assert False, "Shouldn't get here"

    def next():
        if (len(self.children) > 0):
            #random.choice(self.children)
            self._weighted_choice(children)
        else:
            return None


# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:

