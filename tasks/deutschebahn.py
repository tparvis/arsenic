#!/usr/bin/env python3
import IPython
import re
import random
import time
import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from .Task import *


class DeutscheBahn(Task):
    def home(self):
        self.dr.get("http://www.bahn.de")

    def is_open(self):
        return re.match(r'https?://(www\.)?(bahn\.de|deutschebahn\.com)/.*', self.dr.current_url)

    def search(self, place_from, place_to, date):
        if not self.is_open():
            self.home()
        input_departure_point = self.dr.find_element_by_id("qf-departure-point")
        input_destination_point  = self.dr.find_element_by_id("qf-destination-point")
        input_departure_date  = self.dr.find_element_by_id("qf-departure-date")
        
        # Start
        time.sleep(random.uniform(1,4))
        action = webdriver.ActionChains(self.dr)
        action.move_to_element_with_offset(input_departure_point, 10, 5)
        action.click()
        action.perform()
        time.sleep(random.uniform(0,2))
        self.human_send_keys(place_from)
        self.send_key(Keys.ARROW_DOWN)
        time.sleep(random.uniform(0,2))
        self.human_send_keys("\n")
        
        # Ziel
        time.sleep(random.uniform(0,2))
        action = webdriver.ActionChains(self.dr)
        action.move_to_element_with_offset(input_destination_point, 10, 5)
        action.click()
        action.perform()
        time.sleep(random.uniform(0,2))
        self.human_send_keys(place_to)
        self.send_key(Keys.ARROW_DOWN)
        time.sleep(random.uniform(0,2))
        self.human_send_keys("\n")

        # Datum
        time.sleep(random.uniform(0,2))
        action = webdriver.ActionChains(self.dr)
        action.move_to_element_with_offset(input_destination_point, 10, 5)
        action.click()
        action.perform()
        input_departure_date.clear()
        self.human_send_keys(date.strftime("%d.%m.%Y"))

        # Go
        self.human_send_keys("\n")

        # Verbindungsdetails
        time.sleep(random.uniform(0,2))
        detail_link = random.choice(self.dr.find_elements_by_class_name("iconLink"))
        action = webdriver.ActionChains(self.dr)
        action.move_to_element_with_offset(detail_link, 4, 4)
        action.click()
        action.perform()
        
        #IPython.embed()


if __name__ == "__main__":
    driver = webdriver.Firefox()
    db = DeutscheBahn(driver)
    db.search("Nürnberg", "Hannover", datetime.date(2015,3,25))

# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:

