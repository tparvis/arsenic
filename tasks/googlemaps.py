#!/usr/bin/env python3
import IPython
import re
import random
import time
import logging
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from .Task import *

class GoogleMaps(Task):
    def home(self):
        self.dr.get("http://google.de/maps")

    def search(self, place):
        self.home()
        input_search = self.dr.find_element_by_id('searchboxinput')
        self.focus(input_search)
        self.human_send_keys("%s\n" % place)

    def zoom(self, n):
        logging.info("GoogleMaps.zoom(%d)" % (n,))
        btn_zoomin  = self.dr.find_element_by_class_name('widget-zoom-in')
        btn_zoomout = self.dr.find_element_by_class_name('widget-zoom-out')
        while n > 0:
            btn_zoomin.click()
            time.sleep(random.uniform(0.5,2))
            n -= 1
        while n < 0:
            btn_zoomout.click()
            time.sleep(random.uniform(0.5,2))
            n += 1

    def pan(self, x, y):
        logging.info("GoogleMaps.pan(%d, %d)" % (x, y))
        while y > 0:
            self.send_key(Keys.ARROW_DOWN)
            y -= 1
        while y < 0:
            self.send_key(Keys.ARROW_UP)
            y += 1
        while x > 0:
            self.send_key(Keys.ARROW_LEFT)
            x -= 1
        while x < 0:
            self.send_key(Keys.ARROW_RIGHT)
            x += 1

    def browse(self, place):
        self.search(place)
        num_actions = random.randrange(10, 50)
        for i in range(0, num_actions):
            action = random.choice(['zoom', 'pan', 'pause'])
            if action == "zoom":
                self.zoom(random.randrange(-5,5))
            elif action == "pan":
                self.pan(random.randrange(-10, 10), random.randrange(-10, 10))
            elif action == "pause":
                time.sleep(random.uniform(5,10))

if __name__ == "__main__":
    driver = webdriver.Firefox()
    gm = GoogleMaps(driver)
    gm.search("Regensburg")
    #gm.browse("Tokyo")
    IPython.embed()


# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:

