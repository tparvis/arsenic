#!/usr/bin/env python3
import sys
import time
import random
import json
    
    
def typo(c):
    """
        Returns a typo for the character c
    """
    adjacent = [
        "1234567890",
        "qwertyuiop",
        "asdfghjkl;",
        "zxcvbnm,./"
    ]
    width = len(adjacent[0])
    height = len(adjacent)
    for y in range(0, height):
        for x in range(0, width):
            if adjacent[y][x] == c:
                if random.choice([0,1]) == 0:
                    x = max(0, min(width-1, x + random.choice([1,-1])))
                else:
                    y = max(0, min(height-1, y + random.choice([1,-1])))
                return adjacent[y][x]
    return c


def simulate_typing(text, profile=None, key_function=None):
    def random_sleep(timing):
        t = random.gauss( timing['mean'], timing['stdev'] )
        t = max(t, 0.001)
        time.sleep(t)

    def stdout_key_function(c):
        if c == "\b":
            sys.stdout.write("\b \b")
        else:
            sys.stdout.write(c)
        sys.stdout.flush()

    def press_key(c, next_key=None):
        key_function(c)
        timing = None
        if next_key:
            pair = "%c%c" % (c, next_key)
            if pair in profile['key_pair_timings']:
                timing = profile['key_pair_timings'][pair]
        if not timing:
            timing = profile['average_timing']
        random_sleep(timing)            
    
    if not key_function:
        key_function = stdout_key_function

    if not profile:
        profile = { 
            "average_timing": {
                "mean": 0.3,
                "stdev": 0.2
            },
            "error_rate": 0.05,
            "key_pair_timings": {}
        }

    ptr = 0
    last_error_ptr = None
    state = "TYPE"
    while ptr < len(text) or last_error_ptr:
        if last_error_ptr and state == "TYPE":
            if random.random() > 0.8:
                state = "FIX"
        if state == "FIX":
            if ptr >= last_error_ptr:
                press_key("\b")
                ptr -= 1
            else:
                last_error_ptr = None
                state = "TYPE"
        elif state == "TYPE" and ptr < len(text):
            c = text[ptr]
            ptr += 1
            if random.random() < profile["error_rate"]:
                ct = typo(c)
                if c != ct and not last_error_ptr:
                    last_error_ptr = ptr
                c = ct
            if ptr < len(text)-1:
                next_key = text[ptr+1]
            else:
                next_key = None
            press_key(c, next_key)



if __name__ == '__main__':
    if not len(sys.argv) in (2,3):
        print("usage: %s profile.json [textfile.txt]")
        sys.exit(1)

    profile = json.load(open(sys.argv[1], "r"))
    if len(sys.argv) == 3:
        text = open(sys.argv[2], "r").read()
    else:
        text = """The Industrial Revolution and its consequences have been a disaster for the human race. They have greatly increased the life expectancy of those of us who live in “advanced” countries, but they have destabilized society, have made life unfulfilling, have subjected human beings to indignities, have led to widespread psychological suffering (in the Third World to physical suffering as well) and have inflicted severe damage on the natural world. The continued development of technology will worsen the situation. It will certainly subject human beings to greater indignities and inflict greater damage on the natural world, it will probably lead to greater social disruption and psychological suffering, and it may lead to increased physical suffering even in “advanced” countries."""

    simulate_typing(text, profile)
    
    print()



# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:

