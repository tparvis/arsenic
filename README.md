# arsenic, automated web assistant!

## Development
This is just a rough idea, not a finished program.

**Tip:** insert `IPython.embed()` anywhere in the code to get an interactive shell for experiments.

## Dependencies
    aptitude install python3 python3-pip python3-ipython
    pip3 install --upgrade selenium

    git clone https://gitorious.org/arsenic/arsenic.git

## Usage
Try:

    python3 -m tasks.google
    python3 -m tasks.wikipedia
