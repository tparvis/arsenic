#!/usr/bin/env python3
import sys
import tty, termios
import time
import statistics
import json


def getch():
    fd = sys.stdin.fileno()
    old_settings = termios.tcgetattr(fd)
    try:
        tty.setraw(sys.stdin.fileno())
        ch = sys.stdin.read(1)
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
    return ch


def key_pair_stats(pair_timing):
    # pair_timing = { (key1, key2): [delay1, delay2, ...], ... }
    result = {}
    for pair, times in pair_timing.items():
        result["%s%s" % pair] = { 
            'mean':   statistics.mean(times),
            'stdev':  statistics.stdev(times) if len(times) >= 2 else 0.0
        }
    return result


def average_timing(pair_timing):
    alltimes = []
    for times in pair_timing.values():
        alltimes += times
    return {
        'mean':  statistics.mean(alltimes),
        'stdev': statistics.stdev(alltimes)
    }


if __name__ == "__main__":
    pair_timing = { }
    num_backspace = 0
    num_keys = 0
    stack = []
    print("Type a longer text to generate a typist profile.", file=sys.stderr)
    print("Finish with Control-d.\n", file=sys.stderr)
    while 1:
        c  = getch()
        cn = ord(c)
        if cn in (3,4):   # ctrl-[cd] 
            result = {
                'key_pair_timings': key_pair_stats(pair_timing),
                'error_rate': float(num_backspace) / float(num_keys),
                'average_timing': average_timing(pair_timing)
            }
            print(json.dumps(result, sort_keys=True, indent=4, separators=(',', ': ')))
            sys.exit(0)
        elif cn == 13:    # newline
            print(file=sys.stderr)
        elif cn == 127:   # backspace
            num_backspace += 1
            sys.stderr.write("\b \b")
            sys.stderr.flush()
        else:
            num_keys += 1
            sys.stderr.write(c)
            sys.stderr.flush()
            tnow = time.perf_counter()
            stack.append( (c, tnow) )
            if len(stack) == 2:
                key2, time2 = stack.pop()
                key1, time1 = stack.pop()
                pair = (key1, key2)
                if not pair in pair_timing.keys():
                    pair_timing[pair] = []
                pair_timing[pair].append(time2-time1)

# vim:set expandtab tabstop=4 shiftwidth=4 softtabstop=4 nowrap:

